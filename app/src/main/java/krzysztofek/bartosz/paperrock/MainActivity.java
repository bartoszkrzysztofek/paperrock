package krzysztofek.bartosz.paperrock;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public enum Option {
        ROCK, PAPER, SCISSORS
    }

    public enum Result {
        WIN, LOSS, DRAW
    }

    private int playerScore;
    private int androidScore;

    private Option userSelection;
    private Result gameResult;

    @BindView(R.id.androidPoints)
    TextView androidPoints;

    @BindView(R.id.playerPoints)
    TextView playerPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton buttonRock = (ImageButton) findViewById(R.id.stoneButton);
        ImageButton buttonPaper = (ImageButton) findViewById(R.id.paperButton);
        ImageButton buttonScissors = (ImageButton) findViewById(R.id.scissorsButton);

        buttonRock.setOnClickListener(this);
        buttonPaper.setOnClickListener(this);
        buttonScissors.setOnClickListener(this);

  



    }


    @Override
    public void onClick(View v) {
        ImageView imageView = (ImageView) findViewById(R.id.playersChoiceImage);
        boolean play = true;

        switch (v.getId()) {
            case R.id.stoneButton:
                userSelection = Option.ROCK;
                imageView.setImageResource(R.drawable.kryptonita);
                break;
            case R.id.paperButton:
                userSelection = Option.PAPER;
                imageView.setImageResource(R.drawable.paper);
                break;
            case R.id.scissorsButton:
                userSelection = Option.SCISSORS;
                imageView.setImageResource(R.drawable.scissors);
                break;
        }
        if(play){
            play();
            showResults();



        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
       resetImages();
    }

    private void resetImages(){
        ImageView imageView = (ImageView) findViewById(R.id.playersChoiceImage);
        imageView.setImageResource(R.drawable.question);
        ImageView imageView1 = (ImageView) findViewById(R.id.androidChoiceImage);
        imageView1.setImageResource(R.drawable.question);
    }

    private void showResults(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which){

            }
        });
        if(gameResult == Result.LOSS){
            builder.setMessage("Przegrałeś!");
            androidScore++;
        } else if (gameResult == Result.WIN){
            builder.setMessage("Wygrałeś!");
            playerScore++;
        } else if(gameResult == Result.DRAW){
            builder.setMessage("Remis!");
        }
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void play() {
        int rand = ((int) (Math.random() * 10)) % 3;
        Option androidSelection = null;
        ImageView imageView = (ImageView) findViewById(R.id.androidChoiceImage);
        switch (rand) {
            case 0:
                androidSelection = Option.ROCK;
                imageView.setImageResource(R.drawable.kryptonita);
                break;
            case 1:
                androidSelection = Option.PAPER;
                imageView.setImageResource(R.drawable.paper);
                break;
            case 2:
                androidSelection = Option.SCISSORS;
                imageView.setImageResource(R.drawable.scissors);
                break;
        }
        if (androidSelection == userSelection) {
            gameResult = Result.DRAW;
        } else if (androidSelection == Option.ROCK && userSelection == Option.SCISSORS) {
            gameResult = Result.LOSS;

        } else if (androidSelection == Option.ROCK && userSelection == Option.PAPER) {
            gameResult = Result.WIN;

        } else if (androidSelection == Option.SCISSORS && userSelection == Option.PAPER) {
            gameResult = Result.LOSS;

        } else if (androidSelection == Option.SCISSORS && userSelection == Option.ROCK) {
            gameResult = Result.WIN;

        } else if (androidSelection == Option.PAPER && userSelection == Option.ROCK) {
            gameResult = Result.LOSS;

        } else if (androidSelection == Option.PAPER && userSelection == Option.SCISSORS) {
            gameResult = Result.WIN;
        }
    }
}